import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { HeaderComponent } from './components/header.component';
import { AuthenticatedComponent } from './components/authenticated.component';
import { SidebarComponent } from './components/sidebar.component';
import { FooterComponent } from './components/footer.component';

@NgModule({
    imports: [
        RouterModule,
        FormsModule,
        CommonModule,
        SharedModule,
    ],
    exports: [
        AuthenticatedComponent
    ],
    providers: [],
    declarations: [
        HeaderComponent,
        AuthenticatedComponent,
        SidebarComponent,
        FooterComponent,
    ],
    entryComponents: [
    ]
})

export class LayoutModule { }