import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from "../../shared/services/user.service";
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { environment } from '../../../environments/environment';
import { StorageService } from '../../core/services/storage.service';

@Component({
    selector: 'layout-header',
    templateUrl: './templates/header.component.html'
})
export class HeaderComponent {
    siteName;
    user;
    shop;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private storageService: StorageService,
        private userService: UserService,
        private messageService: MessageFlashService,
    ) { }

    ngOnInit() {
        this.siteName = environment.APP_NAME;
        this.shop = this.storageService.get('x-bundles-shop');
        this.activatedRoute.data.subscribe((data: { user }) => {
            this.user = data.user;
        });
    }

    logout() {
        this.userService.logout();
    }

    changePassword() {
        this.router.navigateByUrl('/user/change-password');
    }

}