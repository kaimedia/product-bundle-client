import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../models/user';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'app-authenticated',
    templateUrl: './templates/authenticated.component.html'
})
export class AuthenticatedComponent implements OnInit, OnDestroy {

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
    ) { }

    ngOnInit() {
        document.querySelector('body').classList.add(...['page-header-fixed', 'page-quick-sidebar-over-content', 'page-container-bg-solid', 'page-sidebar-closed']);
    }

    ngOnDestroy(): void {
        document.querySelector('body').classList.remove(...['page-header-fixed', 'page-quick-sidebar-over-content', 'page-container-bg-solid', 'page-sidebar-closed']);
    }
}