import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'select-country',
    templateUrl: './templates/select-country.component.html'
})
export class SelectCountryComponent implements OnInit {
    @Input() object: any;
    @Input() field: string;
    countryModel: string;
    constructor() { }

    ngOnInit() {
        this.countryModel = this.object[this.field];
    }

    onChange() {
        if (this.countryModel) {
            this.object[this.field] = this.countryModel;
        }
    }

}