import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'select-country-code',
    templateUrl: './templates/select-country-code.component.html'
})
export class SelectCountryCodeComponent implements OnInit {
    @Input() object: any;
    @Input() field: string;
    @Input() disabled: boolean = false;
    @Input() size: string;
    @Input() callback;
    countryModel: string;

    constructor() { }

    ngOnInit() {
        this.countryModel = this.object[this.field];
    }

    onChange() {
        if (this.countryModel) {
            this.object[this.field] = this.countryModel;

            this.callback(this.countryModel);
        }
    }

}