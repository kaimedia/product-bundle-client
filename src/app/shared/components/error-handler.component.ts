import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    moduleId: module.id,
    selector: 'app-err',
    templateUrl: './templates/error-handler.component.html',
})
export class ErrorHandlerComponent implements OnInit, OnDestroy {
    errorCode: number;
    private sub: any;
    constructor(
        private route: Router,
        private activatedRoute: ActivatedRoute,
    ) { }

    ngOnInit() {
        this.sub = this.activatedRoute.params.subscribe(params => {
            this.errorCode = params['code'];
        })

        document.querySelector('body').classList.add('page-404-full-page');
    }

    ngOnDestroy() {
        this.sub.unsubscribe();

        document.querySelector('body').classList.remove('page-404-full-page');
    }
}