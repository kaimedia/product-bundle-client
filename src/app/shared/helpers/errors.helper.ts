export const ErrorHelper = {
    required: (attribute: string = 'This attribute') => {
        return attribute + ' is required.';
    },
    invalid: (attribute: string = 'This attribute') => {
        return attribute + ' not valid.';
    },
    invalidEmail: (attribute: string = 'This attribute') => {
        return attribute + ' format is not valid. Try again';
    },
    minlength: (length: number, attribute: string = 'This attribute') => {
        return attribute + ' must be at least ' + length + ' characters long.';
    },
    maxlength: (length: number, attribute: string = 'This attribute') => {
        return attribute + ' cannot be more than ' + length + ' characters long.';
    },
    pattern: (message: string = '', attribute: string = 'This attribute') => {
        if (message) {
            return message;
        }
        return attribute + ' not valid pattern';
    },
    notequal: (attributeA: string, attributeB: string, message: string = '') => {
        if (message) {
            return message;
        }
        return attributeA + ' not equal ' + attributeB;
    },
    message: (message: string) => {
        return message;
    }
}