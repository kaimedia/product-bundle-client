import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';
import { Bundle } from '../../models/bundle';

@Injectable()
export class BundleService {
    constructor(
        private apiConnector: APIConnector,
    ) {

    }

    list(shop): Observable<any> {
        return this.apiConnector.requestAPI('get', `/bundle_groups/${shop}/list/`);
    }

    get(id, shop): Observable<Bundle> {
        return this.apiConnector.requestAPI('get', `/bundle_groups/${shop}/${id}`);
    }

    create(data) {
        return this.apiConnector.requestAPI('post', '/bundle_groups', data);
    }

    update(id, shop, data): Observable<any> {
        data.updated_at = new Date();
        return this.apiConnector.requestAPI('patch', `/bundle_groups/${shop}/${id}`, data)
    }

    status(id, shop, status): Observable<any> {
        return this.apiConnector.requestAPI('put', `/bundle_groups/${shop}/${id}`, { status: status });
    }

    remove(id, shop) {
        return this.apiConnector.requestAPI('delete', `/bundle_groups/${shop}/${id}`);
    }

    upFile(file, shop) {
        return this.apiConnector.requestAPI('post', `/file/${shop}`, file);
    }
}