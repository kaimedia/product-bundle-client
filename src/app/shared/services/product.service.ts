import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ProductService {
    constructor(
        private apiConnector: APIConnector,
    ) {

    }

    list(shop, bundleType, productType = ''): Observable<any> {
        return this.apiConnector.requestAPI('get', `/products/${shop}/${bundleType}/list/${productType}`);
    }

}