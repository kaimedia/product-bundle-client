import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subscription } from 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { StorageService } from '../../core/services/storage.service';

import * as moment from "moment-timezone";
import * as _ from 'lodash';

const dateFormat = 'YYYY-MM-DD';
moment.tz.setDefault(environment.timeZone);

@Component({
    selector: 'app-dashboard',
    templateUrl: './templates/dashboard.component.html',
})

export class DashboardComponent implements OnInit, OnDestroy {
    title: string = "Dashboard";
    subscription: Subscription;
    shopDomain: string;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private storageService: StorageService,
        private messageService: MessageFlashService,
    ) {

    }

    ngOnInit() {
    }


    ngOnDestroy() {
        this.subscriptionInit();
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}