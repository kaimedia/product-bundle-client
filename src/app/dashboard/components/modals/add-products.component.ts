import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs/Subject';
import { Observable, Subscription } from 'rxjs/Rx';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { ProductService } from '../../../shared/services/product.service';
import * as _ from 'lodash';

@Component({
    selector: 'modal-add-products',
    templateUrl: './templates/add-products.component.html',
})

export class AddProductModalComponent implements OnInit, OnDestroy {
    subs: Subscription;
    title: string;
    button: string;
    onClose: Subject<boolean>;
    onComplete: Subject<Array<any>>;
    showLoading: boolean = true;
    shopDomain: string;
    selectedProducts: Array<any> = [];
    allProducts: Array<any> = [];
    productList: Array<any> = [];
    queryProduct: string;
    equals: Array<any> = [];
    whereIn: string = "";
    isEqual: string = "";
    bundleType: string;
    addProductType: string;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalRef: BsModalRef,
        private messageService: MessageFlashService,
        private productService: ProductService,
    ) {

    }

    ngOnInit() {
        this.onClose = new Subject();
        this.onComplete = new Subject();
    }

    showModal(shopDomain: string, body: any, bundleType: string, addProductType: string = '0'): void {
        this.selectedProducts = body ? body : this.selectedProducts;
        this.shopDomain = shopDomain;
        this.bundleType = bundleType;
        this.addProductType = addProductType;
        this.getProducts();
    }

    onSubmit() {
        if (!this.canSubmit()) {
            return;
        }

        this.onClose.next(true);
        this.modalRef.hide();
        this.onComplete.next(this.selectedProducts);
    }

    canSubmit() {
        if (Object.keys(this.selectedProducts).length < 0) {
            return this.messageService.flashError('Please select a least a product.');
        }

        if (Object.keys(this.selectedProducts).length > 5) {
            return this.messageService.flashError('Max selected product is 5.');
        }

        return true;
    }

    getProducts() {
        this.subs = this.productService.list(this.shopDomain, this.bundleType, this.addProductType).subscribe(results => {
            this.allProducts = this.productList = _.map(results, element => {
                return {
                    id: element.id,
                    title: element.title,
                    handle: element.handle,
                    variants: this.searchVariants(element.variants).originVariants,
                    new_price: Number(element.variants[0].price),
                    old_price: Number(element.variants[0].price),
                    image_url: element.image ? element.image.src : "",
                    compare_at_price: element.variants[0].compare_at_price,
                    product_type: element.product_type,
                    disabled: element.disabled,
                    vendor: element.vendor,
                    recommend: {
                        image: element.image ? this.customImageSize(element.image.src) : "",
                        text: '',
                        show: true,
                    },
                    images: element.images,
                };
            });
            this.showLoading = false;
        }, err => this.messageService.flashError(err));
    }

    searchVariants(variants) {
        let originVariants = [];
        let customVariants = [];

        variants.forEach(e => {
            let pattOrigin = new RegExp(/[!@#$%^&*()_+=\[\]{};':"\\|,.<>?]/g);
            if (!pattOrigin.test(e.title)) {
                originVariants.push(e);
            }
        });

        return { originVariants: originVariants };
    }

    customImageSize(path, size = 'medium') {
        var fileName = path.split('/').pop();
        var f_name = fileName.split('.').slice(0, -1).join('.');
        var f_ext = fileName.split('.').pop();
        return path.replace(f_name, f_name + '_' + size);
    }

    searchProduct() {
        this.productList = _.filter(this.allProducts, element => {
            const title = _.toLower(element.title);
            const query = _.toLower(this.queryProduct);
            if (_.includes(title, query)) {
                return true;
            }
            return false;
        });
    }

    changeWhereIn() {
        this.equals = [];
        this.productList = this.allProducts;
        if (this.whereIn === "product_types") {
            this.allProducts.forEach(element => {
                if (element.product_type && !_.includes(this.equals, element.product_type)) {
                    this.equals.push(element.product_type);
                }
            });
        } else if (this.whereIn === "vendors") {
            this.allProducts.forEach(element => {
                if (element.vendor && !_.includes(this.equals, element.vendor)) {
                    this.equals.push(element.vendor);
                }
            });
        }
    }

    changeEquals() {
        if (!this.isEqual) {
            this.productList = this.allProducts;
        }
        else if (this.whereIn === "product_types") {
            this.productList = _.filter(this.allProducts, element => this.isEqual == element.product_type);
        }
        else if (this.whereIn === "vendors") {
            this.productList = _.filter(this.allProducts, element => this.isEqual == element.vendor);
        }
    }

    selectProduct(item) {
        if (this.inBundles(item))
            return;

        if (_.find(this.selectedProducts, { id: item.id })) {
            _.remove(this.selectedProducts, e => e.id === item.id);
        } else {
            this.selectedProducts.push(item);
        }

        return false;
    }

    onSelected(item) {
        if (_.find(this.selectedProducts, { id: item.id }) && !this.inBundles(item)) {
            return true;
        }
        return false;
    }

    alreadySelected(item) {
        const e = _.find(this.selectedProducts, { id: item.id });
        if (e && e.product_quantity && !this.inBundles(item)) {
            return true;
        }
        return false;
    }

    inBundles(item) {
        if (item.disabled) {
            return true;
        }
        return false;
    }

    ngOnDestroy() {
        this.subsInit();
    }

    close(): void {
        this.selectedProducts = _.filter(this.selectedProducts, e => e.product_quantity);
        this.onClose.next(null);
        this.modalRef.hide();
        this.onComplete.next(this.selectedProducts);
    }

    subsInit() {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

}