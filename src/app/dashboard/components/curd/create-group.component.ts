import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { Observable, Subscription } from 'rxjs/Rx';
import { environment } from '../../../../environments/environment';
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AddProductModalComponent } from '../modals/add-products.component';
import { BundleService } from '../../../shared/services/bundle.service';
import * as _ from 'lodash';

@Component({
    selector: 'create-group',
    templateUrl: './templates/create-bundle-group.component.html',
})

export class CreateBundleGroupComponent implements OnInit, OnDestroy {
    title: string = "Create bundle";
    subs: Subscription;
    loading: boolean = true;
    shopDomain: string;
    bundleTypes: Array<any> = environment.bundleTypes;
    bundleType: string = "";
    bundleGroupId: string;
    bundleGroup: any = {
        type: "",
        name: "Group",
        products: "",
        discount_method: 'percentage',
        percent: "",
        fixed_price: "",
        total: 0,
        old_total: 0,
        status: 0,
        shop: "",
        cross_sell: {
            triggerProducts: [],
            offerProducts: [],
            discountValue: "",
        },
        offer: {
            products: [],
            price_levels: [],
            hasFree: false,
            hasSale: false,
        }
    };
    addOriginQty: number;
    addFreeQty: number;
    addValue: number;

    constructor(
        private _location: Location,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageFlashService,
        private modalService: BsModalService,
        private bundleService: BundleService,
    ) {

    }

    ngOnInit() {
        this.activatedRoute.data.subscribe(data => {
            if (data.title) this.title = data.title;
            this.shopDomain = data.shop;
            this.activatedRoute.params.subscribe(params => {
                if (params.id) {
                    this.bundleGroupId = params.id;
                    this.bundleService.get(this.bundleGroupId, this.shopDomain).subscribe(result => {
                        result.cross_sell['offerProducts'] = this.retaskProductRecommend(result.cross_sell['offerProducts']);
                        this.bundleGroup = result;
                        this.bundleType = this.bundleGroup.type;
                        this.loading = false;
                    }, err => {
                        this.messageService.flashError(err);
                    });
                } else {
                    this.bundleGroup.shop = data.shop;
                    this.loading = false;
                }

            });
        });
    }

    retaskProductRecommend(products) {
        return _.map(products, p => {
            if (!p.recommend) p.recommend = {
                image: '',
                text: '',
                show: false,
            };
            return p;
        });
    }

    fileEvent(event: any, item) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let files = event.target.files;
            let file = files[0];
            reader.readAsDataURL(file);
            this.loading = true;
            reader.onload = () => {
                const data = {
                    filename: file.name,
                    filetype: file.type,
                    value: reader.result,
                };
                this.bundleService.upFile(data, this.shopDomain).subscribe(res => {
                    this.loading = false;
                    item.recommend.image = res.url;
                }, err => {
                    this.messageService.flashError(err);
                });
            };
        }
    }

    resetRecommendImage(item) {
        item.recommend.image = this.customImageSize(item.image_url);
    }

    onSubmit() {
        if (!this.canSubmit())
            return;

        this.loading = true;

        this.checkOfferFlag();

        const obversable = this.bundleGroupId
            ? this.bundleService.update(this.bundleGroupId, this.shopDomain, this.bundleGroup)
            : this.bundleService.create(this.bundleGroup);
        obversable.subscribe(res => {
            this.messageService.flashSuccess('Success.');
            this.router.navigate(['/bundles']);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        if (!this.bundleGroup.name) {
            return this.messageService.flashError("Internal name is required.");
        }

        if (this.bundleType == this.bundleTypes[0].id) {
            if ((this.bundleGroup.discount_method === "percentage") && (this.bundleGroup.percent < 0 || this.bundleGroup.percent > 100)) {
                return this.messageService.flashError("Percentage range from 0 to 100%.");
            }

            if (this.bundleGroup.discount_method === "fixed_amount") {
                if (this.bundleGroup.fixed_price < 0) {
                    return this.messageService.flashError("Fixed price not less than 0.");
                }
            }

            if (this.bundleGroup.products.length <= 1) {
                return this.messageService.flashError("Please add more product.");
            }
        }

        if (this.bundleType == this.bundleTypes[1].id) {
            if (Object.keys(this.bundleGroup.offer.products).length == 0) {
                return this.messageService.flashError("Add Products is required.");
            }
            if (Object.keys(this.bundleGroup.offer.price_levels).length == 0) {
                return this.messageService.flashError("Price Levels is required.");
            }
        }

        if (this.bundleType == this.bundleTypes[2].id) {
            if (Object.keys(this.bundleGroup.cross_sell.triggerProducts).length == 0) {
                return this.messageService.flashError("Add Trigger products is required.");
            }

            if (Object.keys(this.bundleGroup.cross_sell.offerProducts).length == 0) {
                return this.messageService.flashError("Add Offer products is required.");
            }

            if (!this.bundleGroup.cross_sell.discountValue) {
                return this.messageService.flashError("Discount value is required.");
            }

            if (this.bundleGroup.cross_sell.discountValue < 0 || this.bundleGroup.cross_sell.discountValue > 100) {
                return this.messageService.flashWarning("Discount value range from 0 to 100.");
            }
        }

        return true;
    }

    checkOfferFlag() {
        if (this.bundleGroup.offer.price_levels.find(function (e) { return e.value == 100 })) {
            this.bundleGroup.offer.hasFree = true;
        } else {
            this.bundleGroup.offer.hasFree = false;
        }

        if (this.bundleGroup.offer.price_levels.find(function (e) { return e.value != 100 })) {
            this.bundleGroup.offer.hasSale = true;
        } else {
            this.bundleGroup.offer.hasSale = false;
        }
    }

    onBundleType(type) {
        this.bundleType = type;
        this.bundleGroup.type = type;
        switch (type) {
            case this.bundleTypes[0].id:
                this.bundleGroup.name = "Group";
                break;

            case this.bundleTypes[1].id:
                this.bundleGroup.name = "Buy 2 Free 1";
                break;

            case this.bundleTypes[2].id:
                this.bundleGroup.name = "Buy X Get Y";
                break;
        }
    }

    onShowBundleTypeList() {
        if (this.bundleGroupId) {
            return true;
        }
        return false;
    }

    onChangePercent(percent) {
        this.bundleGroup.fixed_price = "";
        this.bundleGroup.discount_method = "percentage";
        if (percent < 0 || percent > 100) {
            return;
        }
        this.setNewPriceByPercent(percent);
        const totalPays = this.totalPays();
        this.bundleGroup.total = totalPays;
    }

    onChangeFixedPrice(fixedPrice) {
        this.bundleGroup.percent = "";
        this.bundleGroup.discount_method = "fixed_amount";
        if (fixedPrice < 0) {
            return;
        }
        this.setNewPriceByFixed(fixedPrice);
        this.bundleGroup.total = this.totalPays();
        if (this.bundleGroup.total <= 0) {
            this.bundleGroup.total = 0;
        }
    }

    onChangeOfferPercent(percent) {
        if (percent < 0 || percent > 100) {
            return;
        }
        this.setOfferNewPriceByPercent(percent);
    }

    setNewPriceByPercent(percent) {
        this.bundleGroup.products.forEach(element => {
            const new_price = Number(_.round(element.old_price - ((element.old_price * percent) / 100), 2).toFixed(2));
            element.new_price = new_price > 0 ? new_price : 0;
        });
    }

    setNewPriceByFixed(fixed) {
        this.bundleGroup.products.forEach(element => {
            const new_price = Number((parseFloat(element.old_price) - parseFloat(fixed)).toFixed(2));
            element.new_price = new_price > 0 ? new_price : 0;
        });
    }

    setOfferNewPriceByPercent(percent) {
        this.bundleGroup.cross_sell.offerProducts.forEach(element => {
            const new_price = Number(_.round(element.old_price - ((element.old_price * percent) / 100), 2).toFixed(2));
            element.new_price = new_price > 0 ? new_price : 0;
        });
    }

    delete(index, item) {
        this.bundleGroup.products.splice(index, 1);
        this.bundleGroup.old_total = this.totalRegularPrice();
        this.bundleGroup.total = this.totalPays();
    }

    deleteTriggerProduct(index, item) {
        this.bundleGroup.cross_sell.triggerProducts.splice(index, 1);
    }

    deleteOfferProduct(index, item) {
        this.bundleGroup.cross_sell.offerProducts.splice(index, 1);
    }

    deleteofferProduct(index, item) {
        this.bundleGroup.offer.products.splice(index, 1);
    }

    addProducts() {
        const modal = this.modalService.show(AddProductModalComponent);
        (<AddProductModalComponent>modal.content).showModal(this.shopDomain, this.bundleGroup.products, this.bundleType);
        (<AddProductModalComponent>modal.content).onClose.subscribe(res => {
            (<AddProductModalComponent>modal.content).onComplete.subscribe(results => {
                this.bundleGroup.products = _.map(results, e => {
                    e.product_quantity = 1;
                    return e;
                });
                this.bundleGroup.old_total = this.totalRegularPrice();
                if (this.bundleGroup.percent && this.bundleGroup.percent !== "") {
                    this.setNewPriceByPercent(this.bundleGroup.percent);
                }
                if (this.bundleGroup.fixed_price && this.bundleGroup.fixed_price !== "") {
                    this.setNewPriceByFixed(this.bundleGroup.fixed_price);
                }
                this.bundleGroup.total = this.totalPays();
            });
        });
    }

    addTriggerProducts() {
        const modal = this.modalService.show(AddProductModalComponent);
        (<AddProductModalComponent>modal.content).showModal(this.shopDomain, this.bundleGroup.cross_sell.triggerProducts, this.bundleType, 'trigger');
        (<AddProductModalComponent>modal.content).onClose.subscribe(res => {
            (<AddProductModalComponent>modal.content).onComplete.subscribe(results => {
                this.bundleGroup.cross_sell.triggerProducts = _.map(results, e => {
                    e.product_quantity = 1;
                    return e;
                });
            });
        });
    }

    addOfferProducts() {
        const modal = this.modalService.show(AddProductModalComponent);
        (<AddProductModalComponent>modal.content).showModal(this.shopDomain, this.bundleGroup.cross_sell.offerProducts, this.bundleType, 'offer');
        (<AddProductModalComponent>modal.content).onClose.subscribe(res => {
            (<AddProductModalComponent>modal.content).onComplete.subscribe(results => {
                this.bundleGroup.cross_sell.offerProducts = _.map(results, e => {
                    e.product_quantity = 1;
                    return e;
                });

                if (this.bundleGroup.cross_sell.discountValue && this.bundleGroup.cross_sell.discountValue !== "") {
                    this.setOfferNewPriceByPercent(this.bundleGroup.cross_sell.discountValue);
                }

            });
        });
    }

    addFreeProducts() {
        const modal = this.modalService.show(AddProductModalComponent);
        (<AddProductModalComponent>modal.content).showModal(this.shopDomain, this.bundleGroup.offer.products, this.bundleType);
        (<AddProductModalComponent>modal.content).onClose.subscribe(res => {
            (<AddProductModalComponent>modal.content).onComplete.subscribe(results => {
                this.bundleGroup.offer.products = _.map(results, e => {
                    e.product_quantity = 1;
                    return e;
                });
            });
        });
    }

    addLevel() {
        const addOriginQty = this.addOriginQty;
        const addFreeQty = this.addFreeQty;
        const addValue = this.addValue;

        if (!this.addOriginQty || !this.addFreeQty || !this.addValue) {
            return this.messageService.flashError("Both Qty and Percent Off is required.");
        }
        if (this.addOriginQty < 1) {
            return this.messageService.flashError("Origin Qty must be >= 1.");
        }
        if (this.addFreeQty < 1) {
            return this.messageService.flashError("Free Qty must be >= 1.");
        }
        if (this.addValue < 1 || this.addValue > 100) {
            return this.messageService.flashError("This value from 1 to 100.");
        }
        if (this.bundleGroup.offer.price_levels.find(function (e) { return e.origin_qty == addOriginQty })) {
            return this.messageService.flashError("Can not duplicate Origin Qty.");
        }
        this.bundleGroup.offer.price_levels.push({
            origin_qty: this.addOriginQty,
            free_qty: this.addFreeQty,
            value: this.addValue,
        });
        this.clearAddLevel();
    }

    clearAddLevel() {
        this.addOriginQty = this.addFreeQty = this.addValue = null;
    }

    deleteLevel(index) {
        this.bundleGroup.offer.price_levels.splice(index, 1);
    }

    customImageSize(path, size = 'medium') {
        var fileName = path.split('/').pop();
        var f_name = fileName.split('.').slice(0, -1).join('.');
        var f_ext = fileName.split('.').pop();
        return path.replace(f_name, f_name + '_' + size);
    }

    totalRegularPrice() {
        let totalPrice = 0;
        this.bundleGroup.products.forEach(element => {
            totalPrice += Number(element.old_price);
        });
        return _.round(totalPrice, 2);
    }

    totalPays() {
        let totalPays = 0;
        this.bundleGroup.products.forEach(element => {
            totalPays += Number(element.new_price);
        });
        totalPays = _.round(totalPays, 2).toFixed(2);
        return totalPays;
    }

    goBack() {
        this._location.back();
    }

    ngOnDestroy() {
        this.subsInit();
    }

    subsInit() {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

}