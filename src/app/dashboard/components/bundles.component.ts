import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subscription } from 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from './modals/confirm.component';
import { BundleService } from '../../shared/services/bundle.service';
import * as _ from 'lodash';

@Component({
    selector: 'app-bundle-group',
    templateUrl: './templates/bundles.component.html',
})

export class BundlesComponent implements OnInit, OnDestroy {
    title: string = "Product Bundle Groups";
    subs: Subscription;
    bundleGroups: Array<any> = [];
    shopDomain: string;

    switchStatus = {
        onText: "",
        offText: "",
        onColor: "green",
        offColor: "default",
        size: "small",
        disabled: false,
    };

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private messageService: MessageFlashService,
        private bundleService: BundleService,
    ) {

    }

    ngOnInit() {
        this.activatedRoute.data.subscribe(data => {
            this.shopDomain = data.shop;
            this.list();
        });
    }

    list() {
        this.subs = this.bundleService.list(this.shopDomain).subscribe(results => {
            this.bundleGroups = _.orderBy(results, ['_id'], ['desc']);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    status(item) {
        if (item.status === 0) {
            return 'Off';
        }
        return 'On';
    }

    linkPreview(item) {
        let url;
        const bundleTypes = environment.bundleTypes;
        switch (item.type) {
            case bundleTypes[0].id:
                url = `//${this.shopDomain}/products/${item.products[0].handle}`;
                break;

            case bundleTypes[1].id:
                url = `//${this.shopDomain}/products/${item.offer.products[0].handle}`;
                break;

            case bundleTypes[2].id:
                url = `//${this.shopDomain}/products/${item.cross_sell.triggerProducts[0].handle}`;
                break;
        }
        return url;
    }

    onStatus(item) {
        if (item.status !== 0) {
            return true;
        }
        return false;
    }

    onChangeStatus(status: boolean, item: any) {
        let _status = 0;
        if (status === true) {
            _status = 1;
        }
        this.switchStatus.disabled = true;
        this.bundleService.status(item._id, this.shopDomain, _status).subscribe(() => {
            this.switchStatus.disabled = false;
            this.messageService.flashSuccess("Success");
        }, err => this.messageService.flashError(err));
    }

    delete(item) {
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal("Alert", "Delete this bundle group?");
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.subs = this.bundleService.remove(item._id, this.shopDomain).subscribe(() => {
                    this.messageService.flashSuccess('Success.');
                    this.list();
                }, err => this.messageService.flashError(err));
            }
        });
    }

    ngOnDestroy() {
        this.subsInit();
    }

    subsInit() {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}