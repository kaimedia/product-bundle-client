import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShopGuardService } from '../shared/services/shop-guard.service';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { DashboardComponent } from './components/dashboard.component';
import { BundlesComponent } from './components/bundles.component';
import { CreateBundleGroupComponent } from './components/curd/create-group.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
            title: 'Dashboard',
        }
    },
    {
        path: 'bundles',
        component: BundlesComponent,
        data: {
            title: 'Product Bundle Groups',
        }
    },
    {
        path: 'create_bundle',
        component: CreateBundleGroupComponent,
        data: {
            title: 'Create bundle',
        }
    },
    {
        path: 'edit_bundle/:id',
        component: CreateBundleGroupComponent,
        data: {
            title: 'Edit bundle',
        }
    },
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ],
})

export class DashboardRoutingModule { }