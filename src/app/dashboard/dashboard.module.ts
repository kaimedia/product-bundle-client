import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from "@angular/http";
import { SharedModule } from '../shared/shared.module';
import { DataTableModule } from 'angular2-datatable';
import { ChartsModule } from 'ng2-charts';
import { DashboardRoutingModule } from './dashboard.routing';
import { Select2Module } from 'ng2-select2';
import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';
import { DataFilterPipe } from '../shared/components/data-filter-pipe.component';
import { SortPipe } from "../shared/components/sort-pipe.component";
import { CreateBundleGroupComponent } from './components/curd/create-group.component';
import { ConfirmationModalComponent } from './components/modals/confirm.component';
import { AddProductModalComponent } from './components/modals/add-products.component';
import { DashboardComponent } from './components/dashboard.component';
import { BundlesComponent } from './components/bundles.component';

@NgModule({
    declarations: [
        DataFilterPipe,
        SortPipe,
        ConfirmationModalComponent,
        AddProductModalComponent,
        CreateBundleGroupComponent,
        DashboardComponent,
        BundlesComponent,
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
        SharedModule,
        ChartsModule,
        DashboardRoutingModule,
        DataTableModule,
        Select2Module,
        BootstrapSwitchModule.forRoot(),
    ],
    exports: [

    ],
    providers: [
        FormBuilder,
    ],
    entryComponents: [
        ConfirmationModalComponent,
        CreateBundleGroupComponent,
        AddProductModalComponent,
    ]
})

export class DashboardModule { }