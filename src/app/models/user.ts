export class User {
	_id: string;
	shop: string;
	email: string;
	password: string;
	newPassword: string;
	passwordConfirmation: string;
	firstName: string;
	lastName: string;
	address: string;
	country: string;
	token: string;
	productManagement: object;
	productTypeManagement: object;
	role: string;
	fbAdsId: string;
	active: boolean;
	created_at: Date;
	updated_at: Date;
	ads: {
		spend: number;
	}
	totalSales: number;
	kpiLevel: string;
}
