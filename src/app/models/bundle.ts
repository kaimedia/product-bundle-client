export class Bundle {
    _id: string;
    name: string;
    type: string;
    products: object;
    productIds: string;
    discount_method: string;
    percent: number;
    fixed_price: number;
    total: number;
    old_total: number;
    status: number;
    shop: string;
    created_at: Date;
    updated_at: Date;
    cross_sell: Object;
    offer: Object;
}
