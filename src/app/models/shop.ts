export class Shop {
    _id: string;
    shopify_domain: string;
    custom_domain: string;
    nonce: string;
    isActive: boolean;
    hmac: string;
}
