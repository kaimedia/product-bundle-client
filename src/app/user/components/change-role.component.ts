import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService, User } from '../../shared/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { MessageFlashService } from '../../shared/services/message-flash.service';

@Component({
    moduleId: module.id,
    selector: 'user-change-role',
    templateUrl: './templates/change-role.component.html',
})
export class ChangeRoleComponent implements OnInit, OnDestroy {
    user: User;
    subUser: any;
    constructor(
        private userService: UserService,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageFlashService
    ) { }

    ngOnInit() {
        this.subUser = this.activatedRoute.parent.parent.parent.data.subscribe((data: { user: User }) => {
            this.user = data.user;
        });
    }

    ngOnDestroy() {
        this.subUser.unsubscribe();
    }

    onSubmit() {
        if (!this.canSubmit()) {
            return false;
        }

        const userData = {
            _id: this.user._id,
            role: this.user.role,
        };

        this.userService.update(userData).subscribe(res => {
            this.messageService.flashSuccess('Change role successfully');
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        return this.user.role;
    }

}