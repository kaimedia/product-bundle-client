import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { StorageService } from "../../core/services/storage.service";
import { UserService, User } from '../../shared/services/user.service';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { RegexHelper } from '../../shared/helpers/regex.helper';

@Component({
    selector: 'auth-login',
    templateUrl: './templates/login.component.html',
})

export class LoginComponent implements OnInit {
    title: String = "Please Sign In";
    user: User = new User();
    submitted: boolean = false;
    regexHelper = RegexHelper;
    constructor(
        private userService: UserService,
        private router: Router,
        private storageService: StorageService,
        private messageService: MessageFlashService,
    ) { }

    ngOnInit() {
       
    }

    onSubmit() {
        this.submitted = true;
        if (!this.canSubmit()) {
            return;
        }
        return this.userService.login(this.user, this.callback.bind(this));
    }

    callback(errMsg, response) {
        if (errMsg) {
            this.messageService.flashError(errMsg);
            return;
        }

        const returnUrl = this.storageService.get('return-url');
        if (returnUrl) {
            this.storageService.remove('return-url');
            this.router.navigateByUrl(returnUrl);
            return;
        }
        this.router.navigate(['/dashboard']);
    }

    canSubmit() {
        return this.user.email && this.user.password;
    }
}