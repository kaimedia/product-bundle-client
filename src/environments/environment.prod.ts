const config = require('./config.json');
export const environment = {
  production: true,
  APP_NAME: config.APP_NAME,
  API_ROOT: "https://api-bundles.bluesails.net",
  API_PATH: "https://api-bundles.bluesails.net/api",
  timeZone: config.timeZone,
  bundleTypes: config.bundleTypes
};
